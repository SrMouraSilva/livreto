import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import { Musica } from '../../model/Musica';


export const BibliotecaApi = createApi({
  baseQuery: fetchBaseQuery({baseUrl: "https://api.canta.app/v1/musicas"}),
  refetchOnReconnect: false,

  endpoints: builder => ({
    getMusicaById: builder.query<Musica[], number>({
      query: (id) => ({
        url: `/${id}`,
      }),
    }),
  }),

});
