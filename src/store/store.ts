import {configureStore} from '@reduxjs/toolkit';
import { BibliotecaApi } from './Biblioteca/Biblioteca.api';

export const store = configureStore({
  reducer: {
    // APIs
    [BibliotecaApi.reducerPath]: BibliotecaApi.reducer,
  },

  middleware: gDM => gDM().concat(BibliotecaApi.middleware),
});
