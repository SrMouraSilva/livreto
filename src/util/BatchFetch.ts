import type { Musica } from "../model/Musica";

type Request<In, Out> = (input: In) => Promise<Out>;

/**
 * Based on
 * https://dev.to/karataev/handling-a-lot-of-requests-in-javascript-with-promises-1kbb
 */
export class BatchFetch {
  private constructor() {}

  static async fetch(urls: string[], chunkSize=5): Promise<Musica[]> {
    const fn = async (url: string) => (await fetch(url)).json()

    return await this.fetchChunk(urls, fn, chunkSize);
  }

  private static async fetchChunk<In, Out>(items: In[], fn: Request<In, Out>, chunkSize: number): Promise<Out[]> {
    let result: Out[] = [];

    const chunks = this.splitToChunks(items, chunkSize);

    for (const chunk of chunks) {
      result = [
        ...result,
        ...await this.all(chunk, fn)
      ]
    }

    return result;
  }

  private static splitToChunks<T>(items: T[], chunkSize: number) {
    const result: T[][] = [];

    for (let i = 0; i < items.length; i += chunkSize) {
      result.push(items.slice(i, i + chunkSize));
    }

    return result;
  }

  private static async all<In, Out>(items: In[], fn: Request<In, Out>) {
    const promises = items.map(item => fn(item));
    return await Promise.all(promises);
  }
}