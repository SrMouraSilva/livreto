import { Musica } from "../../model/Musica";

export interface SeparadorVerso {
  prepare(musica: Musica): string;
  apply(letra: string): string;
}

export class SeparadoresVerso {
  public static readonly BARRA: SeparadorVerso = {
      prepare: (musica: Musica) => {
        const delimiter = "||"

        const letra = musica.letra.replace(/(\n\s\s+)/gm, delimiter);

        return letra.split('\n\n')
          .map(it => {
            if (it.startsWith("*")) {
              return it.replace(/\n/g, "||");
            } else {
              return it
            }
          })
          .join('\n\n');
      },
      apply: (letra: string) => {
        const barraHtml = `<span class="verse-delimiter"> / </span>`;
        return letra.replace(/\|\|/gm, barraHtml);
      }
    }

  public static readonly QUEBRA_LINHA: SeparadorVerso = {
    prepare: musica => musica.letra,
    apply: letra => letra,
  }
}