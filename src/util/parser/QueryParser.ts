import { RepertorioQuery } from "../../model/Repertorio";
import { Base64Unicode } from "../Base64Unicode";

export class QueryParser {
  static parse(): RepertorioQuery {
    const params = new URLSearchParams(window.location.search);

    const demonstracao: RepertorioQuery = {
      titulo: "Demonstração",
      itens: [{id: 1, "momento": "Entrada"}]
    }

    if (params.get("repertorio") !== null) {
      try {
        return Base64Unicode.decode(params.get("repertorio")!) as RepertorioQuery;
      } catch (e) {
        return {
          titulo: `Não foi possível gerar lista\nErro: ${e}`,
          itens: [
            {id: 1, "momento": "Música exemplo", tom: "Mi de \"E\"xemplo"}
          ]
        }
      }

      return {
        titulo: "Missa 04/06/2024",
        itens: [
          {id: 5, "momento": "entrada", tom: "C"}, {id: 26, momento: "Ato Penintencial", tom: "C"}, {id: 398, "momento": "final", "tom": "D"}, {id: 568}, {id: 2032}, {id: 1234},
          {id: 2658}, {id: 65}, {id: 958}, {id: 476}, {id: 6}, {id: 74}, {id: 321}, {id: 357}]
      }
    }

    return demonstracao;
  }
}
