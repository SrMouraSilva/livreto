import { ItemRepertorio, Repertorio } from "../../model/Repertorio";
import { Markdown } from "../Markdown";
import { SeparadorVerso } from "./SeparadorVerso";


export class MarkdownParser {

  public constructor(
    private readonly separadorVerso: SeparadorVerso
  ) {}

  public parseMusicas(repertorio: Repertorio) {
    const content = repertorio.itens.map(it => this.parseMusica(it)).join("");

    const header = `<div id="header"><h1>${repertorio.titulo}</h1></div>`;

    const propaganda = `<div class="advertisement">
      <p>Acesse essas e outras músicas no aplicativo <strong>Canta Igreja</strong> disponível para Android.</p>
      <img class="qr-code" src="https://quickchart.io/qr?text=https%3A%2F%2Fcanta.app%2Fandroid&size=140&margin=2" />
    </div>`
    
    const rodape = `<div id="footer">Folheto criado a partir do aplicativo Android <strong>Canta Igreja</strong>.</div>`
    
    return `${header}<div class="main">${content}</div>${propaganda}${rodape}`;
  }

  public parseMusica(itemRepertorio: ItemRepertorio) {
    if (itemRepertorio.musica === undefined) {
      return "";
    }

    const letra = this.separadorVerso.prepare(itemRepertorio.musica);

    const partesLetra = letra.split('\n\n');

    const primeiraParteRenderizada = Markdown.render(`${partesLetra[0]}`);
    const inicioRenderizado = `<div class="music-start">${this.renderCabecalho(itemRepertorio)}${primeiraParteRenderizada}</div>`;

    const restoLetraRenderizado = Markdown.render(partesLetra.slice(1).join('\n\n'));

    const esqueleto = `${inicioRenderizado}\n\n${restoLetraRenderizado}`;

    const renderizado = this.separadorVerso.apply(esqueleto);

    return `<div class="musica">${renderizado}</div>`;
  }

  private renderCabecalho(itemRepertorio: ItemRepertorio) {
    if (itemRepertorio.musica === undefined) {
      return "";
    }

    const musica = itemRepertorio.musica;

    const momento = itemRepertorio.momento ? itemRepertorio.momento.toUpperCase() : undefined;
    const momentoRenderizado = momento
      ? `<div class="music-moment">${momento}</div>`
      : '';

    const tituloRenderizado = Markdown.render(`### ${musica.titulo}`);

    const tom = itemRepertorio.tonalidade
      ? `<div class="tonality">Tom: ${itemRepertorio.tonalidade}</div>`
      : '';

    const compositores = musica.colaboradores.compositores
      ? musica.colaboradores.compositores.join('; ')
      : '';
    const letristas = musica.colaboradores.letristas
      ? "L.: " + musica.colaboradores.letristas.join('; ')
      : '';
    const musicistas = musica.colaboradores.musicistas
      ? "M.: " + musica.colaboradores.musicistas.join('; ')
      : '';

    const hasColaboradores =
         (musica.colaboradores?.compositores?.length ?? 0) > 0 
      || (musica.colaboradores?.letristas?.length ?? 0) > 0
      || (musica.colaboradores?.musicistas?.length ?? 0) > 0
    const colaboradores = hasColaboradores
      ? `<div class="composers">
        <div class="composers-lyrics">${compositores}</div>
        <div class="composers-lyrics">${letristas}</div>
        <div class="composers-music">${musicistas}</div>
      </div>`
      : '';
    
    return `${momentoRenderizado}${tituloRenderizado}${tom}${colaboradores}`
  }
}