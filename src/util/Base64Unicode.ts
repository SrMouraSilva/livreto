/**
 * Based on https://developer.mozilla.org/en-US/docs/Glossary/Base64#the_unicode_problem
 */
export class Base64Unicode {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  static encode(term: any): string {
    return this.bytesToBase64(new TextEncoder().encode(JSON.stringify(term)));
  }

  private static bytesToBase64(bytes: Uint8Array): string {
    const binString = Array.from(bytes, it => String.fromCodePoint(it)).join("");
    return btoa(binString);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  static decode(term: string): any {
    return JSON.parse(new TextDecoder().decode(this.base64ToBytes(term)));
  }

  private static base64ToBytes(base64: string) {
    const binString = atob(base64);
    return Uint8Array.from(binString, (m) => m.codePointAt(0)!);
  }
}