export interface Musica {
    id_musica: number
    titulo: string
    letra: string
    tonalidade_sugerida: string|null,
    colaboradores: {
        compositores?: string[],
        letristas?: string[],
        musicistas?: string[],
    }
    midias: {
        YouTube?: string
    },
    crc32: number
}