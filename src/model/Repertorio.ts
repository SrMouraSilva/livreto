import { Musica } from "./Musica";

export interface Repertorio {
  titulo: string;
  itens: ItemRepertorio[];
}

export interface ItemRepertorio {
  musica?: Musica;

  termo?: string;
  momento?: string;
  tonalidade?: string;
}

export interface RepertorioQuery {
  titulo: string;
  itens: ItemRepertorioQuery[];
}

export interface ItemRepertorioQuery {
  id?: number;
  termo?: string;
  momento?: string;
  tom?: string;
}