import React, { FC, Suspense } from 'react';
import { PreviewerProps } from './Previewer.props';

const Previewer = React.lazy(() => import('./Previewer'));

export const PreviewerLazy: FC<PreviewerProps> = (props) => {
  return (
      <Suspense fallback={<div>Carregando...</div>}>
        <Previewer {...props} />
      </Suspense>
  );
};