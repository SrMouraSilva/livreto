import { FC } from 'react';
import { MarkdownParser } from '../util/parser/MusicParser';
import { usePreviewer } from './usePreviewer';
import './Previewer.css';
import './Previewer.paged.css';
import { PreviewerProps } from './Previewer.props';


const Previewer: FC<PreviewerProps> = ({separador, repertorio}) => {
  const musicas = new MarkdownParser(separador).parseMusicas(repertorio);

  usePreviewer(musicas);

  return <div id="previewer"></div>;
}

export default Previewer;