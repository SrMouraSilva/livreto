import { Previewer } from 'pagedjs';
import { useCallback, useLayoutEffect } from 'react';


export const usePreviewer = (markdown: string) => {
  const resizeContent = useCallback(() => {
    const pages = document.querySelector(".pagedjs_pages") as HTMLElement;

    if (pages) {
      const scale = window.innerWidth*.9 / pages.offsetWidth;

      if (scale < 1) {
        pages.style.transform = `scale(${scale})`;
        pages.style.transformOrigin = 'center top';
      } else {
        pages.style.transform = "none";
      }
    }
  }, []);

  useLayoutEffect(() => {
    const target = document.querySelector("#previewer");
    const stylesheets = ['./folheto.css', './folheto.page.css'];

    if (target) {
      target.innerHTML = ''
    }

    const previewer = new Previewer();

    previewer.on("rendering", () => resizeContent());
    previewer
      .preview(markdown, stylesheets, target)
      .then((flow: {total: number}) => {
        console.log("preview rendered, total pages", flow.total, { flow });
        resizeContent();
      });

    window.addEventListener("resize", resizeContent, false);

    return () => {
      window.removeEventListener("resize", resizeContent, false);

      document.head
        .querySelectorAll('[data-pagedjs-inserted-styles]')
        .forEach((e) => e.parentNode?.removeChild(e))
    }
  }, [markdown, resizeContent]);
}