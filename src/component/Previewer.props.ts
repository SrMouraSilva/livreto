import { Repertorio } from "../model/Repertorio";
import { SeparadorVerso } from "../util/parser/SeparadorVerso";

export interface PreviewerProps {
  repertorio: Repertorio;
  
  separador: SeparadorVerso;
}
