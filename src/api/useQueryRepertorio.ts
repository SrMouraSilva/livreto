import { useCallback, useEffect, useState } from "react";
import { Musica } from "../model/Musica";
import { Repertorio, RepertorioQuery } from "../model/Repertorio";
import { BatchFetch } from "../util/BatchFetch";
import { QueryParser } from "../util/parser/QueryParser";

export const useQueryRepertorio = () => {
  const [content, setContent] = useState<Repertorio|undefined>(undefined);

  const queryMusicas = useCallback(async () => {
    const repertorioQuery = QueryParser.parse();
    const repertorio: Repertorio = {
      titulo: repertorioQuery.titulo,
      itens: []
    }

    const itens = await fetchMusicas(repertorioQuery);

    for (const item of repertorioQuery.itens) {
      if (item.id === undefined) {
        continue;
      }

      repertorio.itens.push({
        musica: itens[item.id],

        termo: item.termo,
        momento: item.momento,
        tonalidade: item.tom,
      })
    }

    setContent(repertorio);
  }, [setContent]);

  useEffect(() => {queryMusicas().then(() => {})}, [queryMusicas]);

  return content;
}

const fetchMusicas = async (query: RepertorioQuery) => {
  console.log(query.itens)
  const urls = query.itens
    .filter(item => item.id)
    .map(item => `https://api.canta.app/v1/musicas/${item.id}`);

  const response = await BatchFetch.fetch(urls, 5);
  const itens: {[key: number]: Musica} = {}
  response.forEach(it => { itens[it.id_musica] = it });

  return itens;
}