import { SeparadoresVerso } from './util/parser/SeparadorVerso';
import { useQueryRepertorio } from './api/useQueryRepertorio';
import { useCallback, useEffect, useMemo } from 'react';
import { PreviewerLazy } from './component/Previewer.lazy';


export function App() {
  const repertorio = useQueryRepertorio();

  const imprimir = useCallback(() => window.print(), []);
  const preview = useMemo(() => repertorio && <PreviewerLazy repertorio={repertorio} separador={SeparadoresVerso.QUEBRA_LINHA} />, [repertorio])
  
  useEffect(() => {
    if (repertorio) {
      document.title = `${repertorio.titulo} - Canta Igreja`;
    }
  }, [repertorio]);

  return (
    <>
      <div className='no-print max-w-screen-xl mx-auto px-5 pt-20 pb-5'>
        <header className="
            p-2 sm:px-5
            fixed top-0 left-0 right-0 z-50 bg-slate-50
            pt-5 pb-3 shadow-md">
          <div className="max-w-screen-xl mx-auto flex flex-row justify-between items-center">
            <h1 className="text-lg font-bold text-slate-800 m-0">Livreto - Canta Igreja</h1>

            <button className="border-0 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded cursor-pointer
              flex align-end items-center"
              onClick={imprimir}>
              <svg aria-hidden className="mr-2" width="18" height="15" viewBox="0 0 18 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M13 9L9 13M9 13L5 9M9 13V1" stroke="#F2F2F2" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
                <path d="M1 17V18C1 18.7956 1.31607 19.5587 1.87868 20.1213C2.44129 20.6839 3.20435 21 4 21H14C14.7956 21 15.5587 20.6839 16.1213 20.1213C16.6839 19.5587 17 18.7956 17 18V17" stroke="#F2F2F2" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
              </svg>
              Gerar arquivo
            </button>
          </div>
        </header>
        {/* <fieldset>
          <legend>Tipo</legend>
          <input type="radio" id="folheto" checked disabled /> <label htmlFor="folheto">Folheto</label>
          <input type="radio" id="livreto" /> <label for="radio">Livreto</label>

        </fieldset> */}
        {/* <h2>Prévia</h2> */}
      </div>

      {preview}
    </>
  )
}
